variable "amiid" {
  default = "ami-0940df33750ae6e7f"
}

variable "type" {
  default = "t2.micro"
}

variable "pemfile" { 
  default = "master"
}

variable "volsize" {
  type = number
  default = 8
}

variable "servername" {
  default = "demoserver"
}
